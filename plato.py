#! env python3
from bs4 import BeautifulSoup
import hashlib
import os
import requests
import sys
import tomd

TABLE_OF_CONTENT_URL = 'https://plato.stanford.edu/contents.html'
CACHE_FOLDER = os.path.join('cache')
BASE_DOMAIN = 'https://plato.stanford.edu/'

""" Fetch url, cache it for futher use """
def fetch_and_cache(url: str) -> str:
    
    h = hashlib.sha512()
    h.update(url.encode('ascii'))

    cache_file = os.path.join(CACHE_FOLDER, h.hexdigest())

    if(os.path.exists(cache_file)):
        with open(cache_file) as f:
            return f.read()

    document = requests.get(url)
    if document.status_code == 200:
        with open(cache_file, 'w') as f:
            f.write(document.text)
        return document.text
    raise ValueError("{} not forund".format(url))

""" Parse HTML table of content into an set of url's partials """
def get_toc_entries(toc_document: str) -> set:
    data = list()
    soup = BeautifulSoup(toc_document, 'html.parser')
    for a in soup.find_all('a'):
        if not a.get('href'):
            continue
        if a.get('href').startswith('entries/'):
            data.append(a.get('href'))

    return sorted(set(data))

""" Return an article into a markdown document """
def fetch_article(url: str) -> str:
    document = fetch_and_cache("{}{}".format(BASE_DOMAIN, url))
    soup = BeautifulSoup(document, 'html.parser')
    article = soup.find(id="article-content")
    return tomd.convert(str(article))


if __name__ == "__main__": # called directly as a script
    fh = open('plato.md', 'w')
    for url in get_toc_entries(fetch_and_cache(TABLE_OF_CONTENT_URL)):
        print("Fetching {}".format(url), end="")
        try:
            article = fetch_article(url)
            fh.write(article)
            print("…done")
        except ValueError as e:
            print("…skipping")
            pass
    fh.close()
